# frozen_string_literal: true

class Product
  attr_accessor :name, :sellIn, :price

  def initialize(name, sellIn, price)
    @name = name
    @sellIn = sellIn
    @price = price
  end
end


class CarInsurance
  attr_reader :products

  def initialize(products)
    @products = products
  end

  def update_price
    @products.each do |product|
      case product.name
      when 'Medium Coverage'
        MediumCoverage.update_price(product)
      when 'Full Coverage'
        FullCoverage.update_price(product)
      when 'Low Coverage'
        LowCoverage.update_price(product)
      when 'Mega Coverage'
        MegaCoverage.update_price(product)
      when 'Special Full Coverage'
        SpecialFullCoverage.update_price(product)
      when 'Super Sale'
        SuperSale.update_price(product)
      else
        p "#{self.class} has not implemented method '#{product.name}'"
      end
    end

    @products
  end
end


class SpecialFullCoverage
  def self.update_price(product)
    if product.price < 50
      product.price = product.price + 1
    end

    if product.sellIn < 11 && product.price < 50
      product.price = product.price + 1
    end

    if product.sellIn < 6 && product.price < 50
      product.price = product.price + 1
    end

    product.sellIn = product.sellIn - 1

    if product.sellIn < 0
      product.price = product.price - product.price
    end
  end
end


class FullCoverage
  def self.update_price(product)
    if product.price < 50
      product.price = product.price + 1
    end

    product.sellIn = product.sellIn - 1

    if product.sellIn < 0 && product.price < 50
      product.price = product.price + 1
    end
  end
end


class MegaCoverage
  def self.update_price(product); end
end


class MediumCoverage
  def self.update_price(product)
    if product.price > 0
      product.price = product.price - 1 
    end

    product.sellIn = product.sellIn - 1

    if product.sellIn < 0 && product.price > 0
      product.price = product.price - 1
    end
  end
end


class LowCoverage
  def self.update_price(product)
    if product.price > 0
      product.price = product.price - 1 
    end

    product.sellIn = product.sellIn - 1

    if product.sellIn < 0 && product.price > 0
      product.price = product.price - 1
    end
  end
end


class SuperSale
  def self.update_price(product)
    if product.price > 0
      product.price = product.price - 1 
    end

    product.sellIn = product.sellIn - 1

    if product.sellIn < 0 && product.price > 0
      product.price = product.price - 1
    end
  end
end


@products_at_day_zero = [
  Product.new('Medium Coverage', 10, 20),
  Product.new('Full Coverage', 2, 0),
  Product.new('Low Coverage', 5, 7),
  Product.new('Mega Coverage', 0, 80),
  Product.new('Mega Coverage', -1, 80),
  Product.new('Special Full Coverage', 15, 20),
  Product.new('Special Full Coverage', 10, 49),
  Product.new('Special Full Coverage', 5, 49),
  Product.new('Super Sale', 3, 6)
]


@car_insurance = CarInsurance.new(@products_at_day_zero)


def product_printer(product)
  p "#{product.name}, #{product.sellIn}, #{product.price}"
end


(0..30).each do |day|
  p "-------- day #{day}  --------"
  p 'name, sellIn, price'
  @car_insurance.update_price.each { |product| product_printer(product) }
  puts ''
end