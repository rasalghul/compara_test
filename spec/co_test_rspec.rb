require './co_test'

RSpec.describe Product do
  context 'Create product' do
    it 'must have name, sellIn, price attributes' do
      product = Product.new('Medium Coverage', 10, 20)

      expect(product.name).to eq('Medium Coverage')
      expect(product.sellIn).to eq(10)
      expect(product.price).to eq(20)
    end

    it 'must be writable' do
      product = Product.new('Medium Coverage', 10, 20)
      product.sellIn = 5
      product.price = 10

      expect(product.sellIn).to eq(5)
      expect(product.price).to eq(10)
    end
  end
end

RSpec.describe CarInsurance do
  context 'Push products' do
    it 'can be store multiple products' do
      products_at_day_zero = [
        Product.new('Medium Coverage', 10, 20),
        Product.new('Full Coverage', 2, 0)
      ]

      car_insurance = CarInsurance.new(products_at_day_zero)
      expect(car_insurance.products.length).to eq(2)
    end
  end

  context 'Update prices' do
    it 'must update given product attributes' do
      products = [Product.new('Medium Coverage', 10, 20)]
      car_insurance = CarInsurance.new(products)
      car_insurance.update_price

      expect(car_insurance.products.first.sellIn).not_to eq(10)
      expect(car_insurance.products.first.price).not_to eq(20)

      expect(car_insurance.products.first.sellIn).to eq(9)
      expect(car_insurance.products.first.price).to eq(19)
    end
  end
end