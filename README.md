# Instructions

Build docker image

```bash
$ docker build -t compara_test . > outputs/buildOutput.txt 
```

Run the image

```bash
$ docker run -t compara_test . > outputs/coTestOutput.txt 
```

See results on coTestOutput.txt file.

Tests
```bash
$ rspec spec/co_test_rspec.rb > outputs/testOutput.txt 
```